CentOS 7 VirtualBox Setup
=========================
.. admonition:: Version information

   | **Last updated:** 06/26/2021
   | **Host:** Windows 10 Pro AME version 2004 (64-bit)
   | **VirtualBox version:** 6.1.22 r144080 (Qt5.6.2)

This guide describes a basic CentOS 7 installation on VirtualBox with Guest Additions. You can use the following table of contents to skip ahead to the desired section.

.. contents::
   :local:

VirtualBox Setup
----------------
Choose a close mirror from http://isoredirect.centos.org/centos/7/isos/x86_64/ and download the NetInstall image.

Create a new virtual machine with type Linux and version Red Hat (64-bit). Here is a MiB to GiB conversion table for reference:

===== ===
 MiB  GiB
===== ===
 1024   1
 2048   2
 3072   3
 4096   4
 5120   5
 6144   6
 7168   7
 8192   8
 9216   9
10240  10
11264  11
12288  12
13312  13
14336  14
15360  15
16384  16
===== ===

Set up the virtual machine settings as follows:

1. In the *General → Advanced* tab set Shared Clipboard to Bidirectional
2. In the *System → Motherboard* tab configure the Boot Order so that it boots first from Optical
3. In the *System → Motherboard* tab set Pointing Device to USB Tablet
4. In the *System → Processor* tab check Enable Nested VT-x/AMD-V
5. In the *Audio* tab uncheck Enable Audio
6. In the Advanced section of the *Network* tab configure port fortwarding for the desired adapters
7. In the *Shared Folders* tab add a shared folder with Folder Path ``F:\vbox\share``, Folder Name ``share``, Mount point ``/mnt/share``, and check Auto-mount
8. In the *User Interface* tab uncheck everything except for the uppermost menu

Leave all the unmentioned configurations at their default values.

Start the virtual machine and choose the ``CentOS-7-x86_64-NetInstall-2009.iso`` image previously downlodaded.

CentOS 7 Installation
---------------------
Follow these steps for the operating system installation:

1. In the *KEYBOARD* option add Portuguese (Brazil) and remove English (US)
2. In the *NETWORK & HOST NAME* option set the ON/OFF switch to ON
3. In the *DATE & TIME* option set Region to Americas, City to Sao_Paulo, switch Network Time to OFF, set the time format to AM/PM, and finally switch Network Time back to ON
4. In the *INSTALLATION SOURCE* option leave ``http://`` as the default protocol and use `mirror.ufscar.br/centos/7/os/x86_64 <http://mirror.ufscar.br/centos/7/os/x86_64/>`_ as the mirror

.. important::

   **Do not** add a slash (``/``) to the end of the mirror URL.

5. In the *SOFTWARE SELECTION* option select Minimal Install as the Base Environment and leave all the Add-Ons unchecked
6. In the *INSTALLATION DESTINATION* option just click the Done button
7. In the *KDUMP* option unchek Enable kdump
8. Leave the *SECURITY POLICY* option as it is and click the Begin Installation button
9. In the *ROOT PASSWORD* option set ``123`` as the password and click the Done button twice
10. Do not create any users, wait for the installation to finish, and click the Reboot button

The installer will automatically eject the CD/DVD drive and reboot the virtual machine. You will be then prompted to login. Login as the ``root`` user with the password just set in step 9.

User Creation and Hostname Configuration
----------------------------------------
It is highly recommended to create a non-root user with administrative privileges and set a password for them. To do so, issue the following commands as the ``root`` user, replacing ``centos`` with any other username if desired:

.. code-block:: console

   [root@localhost ~]# useradd centos

.. code-block:: console

   [root@localhost ~]# passwd centos

To grant a user administrative privileges without needing to provide a password periodically, run:

.. code-block:: console

   [root@localhost ~]# echo "centos ALL=(ALL) NOPASSWD: ALL" | tee /etc/sudoers.d/centos

To login as the newly created user, run:

.. code-block:: console

   [root@localhost ~]# su - centos

If wanted, the hostname can be changed. To do so, issue the following command, where ``centos.localdomain`` can be any other hostname:

.. code-block:: console

   [centos@localhost ~]$ sudo hostnamectl set-hostname centos.localdomain

.. note::

   The hostname can either be a FQDN or not.

To view the new hostname in the prompt, logout and login back, or simply run:

.. code-block:: console

   [centos@localhost ~]$ exec bash

Vim
---
.. important::

   Before installing any packages in this or in subsequent sections it is recommended to update the system packages with:

.. code-block:: console

   [centos@centos ~]$ sudo yum -y update

CentOS ships with only vi. To install Vim, run:

.. code-block:: console

   [centos@centos ~]$ sudo yum -y install vim-enhanced

Vim's automatic indentation can be annoying at times, and line numbers are most helpful when editing code. To change Vim's default behavior, run:

.. code-block:: console

   [centos@centos ~]$ vi ~/.vimrc

Add the following lines and save the file:

.. code-block:: vim
   :linenos:

   set noautoindent
   set nocindent
   set nosmartindent
   set number

If you run Vim with ``sudo`` these settings won't be applied. To fix this, copy the vimrc file to the ``root`` user home directory:

.. code-block:: console

   [centos@centos ~]$ sudo cp ~/.vimrc /root

Autocompletion and History
---------------------------
CentOS does not ship with Bash autocompletion by default. We can install it by running:

.. code-block:: console

   [centos@centos ~]$ sudo yum -y install bash-completion

Logout and login back, or simply run:

.. code-block:: console

   [centos@centos ~]$ exec bash

The history of the commands you issue at the terminal have great value as a future reference on how you carried out tasks and solved problems. To prevent the Bash history from being truncated or overwritten over time and across sessions, add the following lines to the bottom of both ``~/.bashrc`` and ``/root/.bashrc`` files:

.. code-block:: vim
   :linenos:

   shopt -s histappend
   HISTCONTROL=erasedups:ignoredups
   HISTSIZE=
   HISTFILESIZE=
   PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

.. warning::

   **Do not** set the ``HISTSIZE`` or ``HISTFILESIZE`` environment variables to ``-1``. Although it works on Ubuntu, on CentOS the Bash history will break for some reason.

And finally run:

.. code-block:: console

   [centos@centos ~]$ source ~/.bashrc

i3 and Konsole
--------------
A windowing system is needed for the VirtualBox Guest Additions to work. Since the minimal install does not provide one, manual installation is required. This guide uses the `X.Org server <https://x.org/wiki/>`_ as the windowing system, `i3 <https://i3wm.org/>`_ as the window manager, and `Konsole <https://konsole.kde.org/>`_ as the terminal emulator, although other configurations are also possible.

To install X.Org, run:

.. code-block:: console

   [centos@centos ~]$ sudo yum -y install 
