docs
====
This is a personal documentation repository containing some guides I made for myself. If you stumbled upon it and found something useful, feel free to share or fork it ;)

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Guides

   vbox/centos
   vbox/ubuntu
